Angular Frontend Project
=========================

Libraries used

 * Standard Angular
 * Standard Javascript 5
 * Plain HTML
 * Bootstrap with "UI Bootstrap" angular bootstrap
 * ngRouter: Enhanced angular router
 * npm packet manager for backend development tools written in javascript
 * bower packet manager for frontend libraries

# Installation

```
   $ cd my_project
   $ npm install
   $ bower install
```

# Development tools

Starting development server on localhost:3000:

```
   $ gulp serve
```
