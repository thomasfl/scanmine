(function() {
  'use strict';

  angular
    .module('scanmine', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngResource', 'ui.router', 'ui.bootstrap']);

})();
