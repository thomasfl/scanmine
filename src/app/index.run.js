(function() {
  'use strict';

  angular
    .module('scanmine')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
